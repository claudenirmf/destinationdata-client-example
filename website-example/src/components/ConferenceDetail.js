import React from "react";
import { Component } from "react";
import { Descriptions, Image, Row } from "antd";
import { SimpleMap } from "./SimpleMap";

export class ConferenceDetail extends Component {
  render() {
    const lng = "11.33171";
    const lat = "46.47861";

    return (
      <>
        <br />

        <Row justify="center">
          <Image
            width={400}
            style={{ textAlign: "center" }}
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/67/NOI_Techpark_S%C3%BCdtirol_7.jpg/800px-NOI_Techpark_S%C3%BCdtirol_7.jpg"
          />
        </Row>

        <br />

        <h1>
          <b>AlpineBits DestinationData Workshop</b>
        </h1>
        <h2>How to integrate DestinationData into your solutions.</h2>

        <p>
          The AlpineBits DestinationData standard provides a novel way for
          sharing in a world of APIs. The standard is thoroughly design to meet
          the demands of businesses interested in provide or consume tourism
          related data in alpine regions. Developed as a joint effort key
          players in the South Tyrolean market, the AlpineBits DestinationData
          standard has among its core concerns quality aspects fundamental for
          the development of reliable solutions, such as consistency and
          flexibility.
        </p>

        <Descriptions
          title={`AlpineBits DestinationData Workshop: How to integrate DestinationData into your solutions.`}
          bordered
          column={1}
        >
          <Descriptions.Item label="Date">
            September 17<sup>th</sup>, 2020 14:00-17:00 CEST
          </Descriptions.Item>

          <Descriptions.Item label="Location">
            NOI TechPark, Bolzano (BZ).
          </Descriptions.Item>
        </Descriptions>

        <br />

        <SimpleMap lat={lat} lng={lng} />
      </>
    );
  }
}
