# DestinationData Client Examples

This repositories contains examples of client applications consuming data published under the DestinationData standard.
The contained projects are the following:

* `./website-example`: a JavaScript React single page application that enriches a website with a list of future events and details on each event and its location.

* `./data-import-example`: a JavaScript project that retrieves data from a DestinationData server and maps into a relational database showing an example of data integration.
